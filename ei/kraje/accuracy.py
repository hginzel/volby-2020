"""Accuracy of EI estimates."""

import csv
import json
import numpy as np
import pandas as pd

data_path = "/home/michal/dev/volebniatlas_backend/geo_data/region/"
path = "/home/michal/dev/prechody/volby-2020/ei/kraje/"

with open(path + "elections.json") as fin:
    elections_obj = json.load(fin)

election_codes = list(elections_obj.keys())

mec = election_codes[0] # 'kraje2020' # main_election_code
election_codes = election_codes[1:] # ['psp2017', 'pres2018-1', 'pres2018-2', 'euro2019', 'kraje2016'] # additionals

# list of regions
region_codes = []
with open(path + "regions.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        region_codes.append(row['code'])

rows = []
header = ["region", "current", "measuring", "abbreviation", "votes", "estimate", "rate"]

# ec = 'kraje2016'
# code = "CZ032"

for ec in election_codes:
    for code in region_codes:

        previous = code + "_" + ec
        current = code + "_" + mec

        with open(data_path + "region_" + code + ".json") as fin:
            geo_data = json.load(fin)

        parties = {
            current: {},
            previous: {}
        }
        plist = parties.keys()
        for y in plist:
            with open(path + "data/list_" + y + "_filtered.csv") as fin:
                dr = csv.DictReader(fin)
                for row in dr:
                    if row['id'] != '':
                        parties[y][row['abbreviation']] = row


        df = pd.read_csv(path + "data/matn_" + code + "_" + mec + "_" + ec + ".csv", index_col=0)

        sums = {
            current: np.sum(df),
            previous: np.sum(df, axis=1)
        }
        for y in parties:
            xec = y.split('_')[1]
            for p in parties[y]:
                votes = int(geo_data['data'][xec]['results'][parties[y][p]['id']])
                estimate = sums[y][p]
                item = {
                    "region": code,
                    "current": mec,
                    "measuring": xec,
                    "abbreviation": p,
                    "votes": votes,
                    "estimate": estimate,
                    "rate": estimate / votes
                }
                rows.append(item)

with open(path + "accuracy.csv", "w") as fout:
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    dw.writerows(rows)
