"""GEnerates table for markdown README."""

import csv
import json

import csv
import json
import random

path = "/home/michal/dev/prechody/volby-2020/ei/kraje/"

with open(path + "elections.json") as fin:
    elections_obj = json.load(fin)

election_codes = list(elections_obj.keys())

mec = election_codes[0] # 'kraje2020' # main_election_code
election_codes = election_codes[1:] # ['psp2017', 'pres2018-1', 'pres2018-2', 'euro2019', 'kraje2016'] # additionals

# list of regions
region_codes = []
regions = {}
with open(path + "regions.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        region_codes.append(row['code'])
        regions[row['code']] = row

table = ""
for code in region_codes:
    table +="| " + regions[code]['name'] + " | "
    interactive = []
    svg = []
    facebook = []
    twitter = []

    for ec in election_codes:
        interactive.append('[' + elections_obj[ec]['name'] + "](https://volebnikalkulacka.cz/docs/ei/volby-2020/" + code + "_kraje2020_" + ec + "_500.html)")
        svg.append('[' + elections_obj[ec]['name'] + "](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/" + code + "_kraje2020_" + ec + "_1000_900.svg)")
        facebook.append('[' + elections_obj[ec]['name'] + "](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_" + code + "_kraje2020_" + ec + "_1360x1360.png)")
        twitter.append('[' + elections_obj[ec]['name'] + "](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_" + code + "_kraje2020_" + ec + "_1014x570.png)")
    
    table += ", ".join(interactive) + " | "
    table += ", ".join(svg) + " | "
    table += ", ".join(facebook) + " | "
    table += ", ".join(twitter) + " |\n"

with open(path + "table_readme_md.txt", "w") as fout:
    fout.write(table)
