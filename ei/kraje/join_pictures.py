"""Add footers for pictures."""

import sys
from PIL import Image

path = "/home/michal/dev/prechody/volby2020/ei/kraje/"

with open(path + "elections.json") as fin:
    elections_obj = json.load(fin)

election_codes = list(elections_obj.keys())

mec = election_codes[0] # 'kraje2020' # main_election_code
election_codes = election_codes[1:] # ['psp2017', 'pres2018-1', 'pres2018-2', 'euro2019', 'kraje2016'] # additionals

sizes = [
    # {"width": 500, "height": 450, "footer": 50},
    # {"width": 750, "height": 425, "footer": 75},
    # {"width": 1000, "height": 400, "footer": 100},
    # {"width": 1000, "height": 900, "footer": 100},
    {"width": 1360,  "height": 1220, "footer": 140}, # Fb
    {"width": 680, "height": 610, "footer": 70}, # Fb
    {"width": 507, "height": 245, "footer": 40}, # Tw
    {"width": 1014, "height": 490, "footer": 80} # Tw
]

# list of regions
region_codes = []
regions = {}
with open(path + "regions.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        region_codes.append(row['code'])
        regions[row['code']] = row

# ec = 'kraje2016'
# code = 'CZ080'
# s = {"width": 500, "height": 450, "footer": 50}
for ec in election_codes:
    print(ec)
    for code in region_codes:
        for s in sizes:

            inames = [
                path + "images/" + code + "_" + mec + "_" + ec + "_" + str(s['width']) + "_" + str(s['height']) + ".png",
                path + "images/footer_" + str(s['width']) + "_" + str(s['footer']) + ".png"
            ]

            images = [Image.open(x) for x in inames]
            widths, heights = zip(*(i.size for i in images))

            max_width = max(widths)
            total_height = sum(heights)

            new_im = Image.new('RGB', (max_width, total_height))

            y_offset = 0
            for im in images:
                new_im.paste(im, (0, y_offset))
                y_offset += im.size[1]

            new_im.save(path + "images/ei_" + code + "_" + mec + "_" + ec + "_" + str(max_width) + "x" + str(total_height) + ".png")
print("done")