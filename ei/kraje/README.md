# Analysis flow

Using VolebniAtlas.cz (local) data for older elections.

1. `prepare_data.py` -> creates `data_...csv` and `list_....csv` files
2. `join_data.py` -> creates `data_...filtered.csv` and `data_...filtered_random.csv` files
3. `ei.py` + `ei.r` -> creates `matn_...csv` ... files
4. `accuracy.py` -> `accuracy.csv`
5. `matrix.py` -> creates matrix images .png, .svg, .html
6. `footer.html` -> manually (using FF: Ctrl+Shift+s) creates footer_w_h.png files
7. `join_pictures.py` -> creates `ei_....png` charts with a footer
