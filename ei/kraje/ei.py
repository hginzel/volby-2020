"""Calculate ecological inference."""

import csv
import json
import numpy
import rpy2.robjects as robjects
r = robjects.r

data_path = "/home/michal/dev/volebniatlas_backend/geo_data/region/"
path = "/home/michal/dev/prechody/volby-2020/ei/kraje/"

with open(path + "elections.json") as fin:
    elections_obj = json.load(fin)

election_codes = list(elections_obj.keys())

mec = election_codes[0] # 'kraje2020' # main_election_code
election_codes = election_codes[1:] # ['psp2017', 'pres2018-1', 'pres2018-2', 'euro2019', 'kraje2016'] # additionals

# list of regions
region_codes = []
with open(path + "regions.csv") as fin:
    dr = csv.DictReader(fin)
    for row in dr:
        region_codes.append(row['code'])

for ec in election_codes:
    for code in region_codes:
    
        # ec = 'kraje2016'
        # code = "CZ032"

        with open(data_path + "region_" + code + ".json") as fin:
            geo_data = json.load(fin)

        print (code, ec)

        robjects.globalenv['path'] = path + "data/"
        robjects.globalenv['Ntotal'] = geo_data['data'][mec]['statistics']['total_voters']
        robjects.globalenv['list_previous_name'] = path + "data/list_" + code + "_" + ec + "_filtered.csv"
        robjects.globalenv['list_current_name'] = path + "data/list_" + code + "_" + mec + "_filtered.csv"
        robjects.globalenv["data_filtered_random"] = path + "data/data_" + code + "_" + mec + "_" + ec + "_filtered.csv"
        robjects.globalenv["matn_filtered_name"] = path + "data/matn_" + code + "_" + mec + "_" + ec + "_filtered.csv"
        robjects.globalenv["matn_chart_filtered"] = path + "data/matn_chart_" + code + "_" + mec + "_" + ec + "_filtered.csv"
        robjects.globalenv["matp_filtered"] = path + "data/matp_" + code + "_" + mec + "_" + ec + "_filtered.csv"
        robjects.globalenv["matn"] = path + "data/matn_" + code + "_" + mec + "_" + ec + ".csv"
        robjects.globalenv["matn_chart"] = path + "data/matn_chart_" + code + "_" + mec + "_" + ec + ".csv"

        try:
            r.source(path + "ei.r")
            # with open(path + "matp_filtered.csv", "w") as fout:
            #     csvw = csv.writer(fout)
            #     for row in numpy.array(robjects.globalenv['matp']):
            #         csvw.writerow(list(row))
            # with open(path + "matn_filtered.csv", "w") as fout:
            #     csvw = csv.writer(fout)
            #     for row in numpy.array(robjects.globalenv['mat_ave']):
            #         csvw.writerow(list(row))
        except Exception:
            print("skipping")