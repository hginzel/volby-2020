# Volby 2020: Přesuny voličů

Navigace:
- [Odhady statistickou metodou ekologické inference](#odhady-statistickou-metodou-ekologické-inference)
    - [Jak číst grafy](#jak-číst-grafy)
- [Koho voliči přišli k volbám](#koho-voliči-přišli-k-volbám)
    - [Voliči z parlamentních voleb 2017](#voliči-z-parlamentních-voleb-2017)
    - [Voliči z prezidentských voleb (2. kolo)](#voliči-z-prezidentských-voleb-(2.-kolo))
- [Přesuny voličů](#přesuny-voličů)
    - [Eurovolby 2019](#eurovolby-2019)
    - [Prezidentské volby 2018 (2.kolo)](#prezidentské-volby-2018-(2.kolo))
    - [Prezidentské volby 2018 (1.kolo)](#prezidentské-volby-2018-(1.kolo))
    - [Parlamentní volby 2017](#parlamentní-volby-2017)
    - [Krajské volby 2016](#krajské-volby-2016)
- [Grafy ke stažení](#grafy-ke-stažení)
- [♥ Díky](#díky-♥)
- [Další projekty autora](#další-projekty-autora)
    - [Mandáty.cz](#mandaty-cz)
    - [Volební atlas](#volební-atlas)
    - [Volební kalkulačka](#volební-kalkulačka)

## Shrnutí
- 65 modelů: 13 krajů x 5 předchozích voleb
- Zásadní pro výsledky voleb bylo, čí voliči přišli k volbám: voliči ODS a KDU-ČSL z roku 2017 přišli skoro všichni, většina voličů ANO a SPD naopak ke krajským volbám nešla.

## Odhady statistickou metodou ekologické inference
Celá analýza je postavena **na statistickém modelu [ekologické inference](http://kohovolit.eu/cs/jak-se-presouvaji-volici-mezi-volbami-ekologicka-inference/)**, který využívá výsledky voleb ze všech volebních okrsků (více jak 13 000 okrsků v krajských volbách v ČR). Model se snaží **co nejlépe přiblížit skutečným výsledkům voleb v těchto okrscích** vždy pro dvojici voleb: Aktuální krajské volby na jedné straně a na druhé: parlamentí volby 2017, nebo eurovolby 2019, nebo prezidentské 2018 (1. a 2. kolo), nebo krajské 2016.

Pro každý kraj byl spočten vlastní model, celkem tedy 13 modelů pro každou dvojici voleb, **celkem tedy 13 (kraje) krát 5 (volby) modelů**. Do modelů vstupovaly strany (kanditáti), které získaly **v daném kraji alespoň 5 % hlasů**, zbytek byl zařazen do skupiny _ostatní_.

Upozornění: Jedná se o statistické modely a všechny vypočtené hodnoty jsou tedy **přibližné**!.

### Jak číst grafy
Modely ukazuji ve formě grafů, kde na ose _x_ je, koho voliči volili v minulých volbách. Na ose _y_ je, koho volili v současných krajských volbách 2020.

Velikost bubliny odpovídá počtu voličů. Barva je dle strany z aktuálních krajských voleb.

Lidé, kteří nevolili ani v jedněch volbách, nejsou v grafu zobrazeni (byla by to zdaleka největší bublina).

#### Příklad: Jihočeský kraj: Sněmovní volby 2017 a krajské volby 2020
Na grafu jsou vyznačeny elipsami: 

- vodorovná elipsa: voliči Pirátů v krajských volbách 2020
- svislá elipsa: voliči ODS ve sněmovních volbách 2017
- bublina v obou elipsách: voliči ODS v 2017, kteří nyní v krajských volbách 2020 volili Piráty

![JČ](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_psp2017_750x500_annotated_example.png)

## Koho voliči přišli k volbám
V krajských volbách je také důležité, jací voliči přijdou, kterým stranám se podaří k volbám dotáhnout "své" voliče.

### Voliči z parlamentních voleb 2017
Vezměme si pro **příklad Jihočeský kraj** a strany **ANO a ODS**. Zde v parlamentních volbách **2017 ANO dostalo přes 90 000** hlasů a **ODS necelých 40 000** hlasů ([výsledky 2017](https://volby.cz/pls/ps2017nss/ps311?xjazyk=CZ&xkraj=3)).

V současných krajských volbách **2020 obě strany dostaly zhruba 35 000** hlasů ([výsledky 2020](https://volby.cz/pls/kz2020/kz21?XJAZYK=CZ&xkraj=2)).

**Obě strany si udržely zhruba 15 000 - 20 000 svých voličů z 2017** a zbytek hlasů získaly od ostatních stran (ODS např. od TOP 09, ANO od KSČM, atd.) a od lidí, co v 2017 nevolili.

**Od obou stran odešlo zhruba 10 000** jejich voličů z roku 2017.

Zásadní rozdíl ale byl v tom, že **voličů ODS z 2017 nepřišlo nyní méně než 10 000** (zhruba 1/5), ale **voličů ANO z 2017 nepřišlo nyní zhruba 50 000 - 60 000** (skoro 2/3). Tyto voliče strany neztratily ve prospěch jiných stran, tito lidé jen nepřišli volit.

![JČ](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_psp2017_750x500_annotated_ano_ods.png)

Za celou ČR (bez Prahy, kde se nyní nevolilo) to u parlamentních stran vypadá zhruba takto:

| Strana | Přišĺo jejich voličů z 2017 | Z nynějších voličů je volilo už v 2017 * |
| ----- | ----- | ----- |
| ANO | 40 % | 60 % |
| ODS | 80 % | 60 % |
| Piráti | 60 % | 40 % |
| SPD | 35 % | 40 % |
| KSČM | 50 % | 55 % |
| ČSSD | 45 % | 40 % |
| KDU-ČSL | 80 % | 75 % |
| TOP 09 | 75 % |  |
| STAN | 75 % | 35 % |

* Pozn.: čísla v posledním sloupci jsou jen za ty kraje, kde strana kandidovala samostatně a překročila 5 %.


### Voliči z prezidentských voleb (2. kolo)
Opět na **příkladu Jihočeského kraje** můžeme vidět **velké rozdíly mezi voliči obou kandidátů** v prezidentských volbách.

**Většina voličů p.Zemana** z druhého kola prezidentských voleb **ke krajských volbám nepřišla**. A ti, co přišli, volili nejvíce ANO, ČSSD nebo KSČM (z parlamentních stran).

Naopak **většina voličů p.Drahoše k volbám přišla** a volila hlavně strany, které jsou ve sněmovně v opozici.

![JČ](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_psp2017_750x500_annotated_president.png)

| Kandidát | Přišĺo jeho voličů z 2018 |
| ----- | ----- |
| Zeman | 40 % |
| Drahoš | 70 % |

## Přesuny voličů
Nyní už přesuny mezi jednotlivými volbami a současnými krajskými volbami ve všech krajích.

### Eurovolby 2019
Mezi největší změny od eurovoleb patří zisky Pirátů mezi voličemi ODS, STAN+TOP 09 a občas i od ANO. Speciálně koalicím STANu se (ač stále to nejsou nijak velké přesuny) získat některé voliče ANO: v Olomouckém kraji (s Piráty) nebo v Plzeňském (se Zelenými a exhejtmanem Bernardem). Ke STANu také nejvíce odešli někteří voliči Pirátů.

Podstatné ale bylo, kteří nevoliči z eurovoleb tentokrát přišli, což se lišilo i kraj od kraje: viz třeba porovnání zisku KDU-ČSL v Zlínském kraji a na Vysočině.
<table>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_euro2019_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_euro2019_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_euro2019_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_euro2019_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_euro2019_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_euro2019_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_euro2019_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_euro2019_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_euro2019_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_euro2019_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_euro2019_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_euro2019_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_euro2019_500x500.png"/></td>
    </tr>
</table>

### Prezidentské volby 2018 (2.kolo)
Viz popis výše u [účasti voličů z prezidentských voleb](#Voliči-z-prezidentských-voleb-(2.-kolo))

<table>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_pres2018-2_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_pres2018-2_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_pres2018-2_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_pres2018-2_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_pres2018-2_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_pres2018-2_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_pres2018-2_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_pres2018-2_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_pres2018-2_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_pres2018-2_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_pres2018-2_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_pres2018-2_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_pres2018-2_500x500.png"/></td>
    </tr>
</table>

### Prezidentské volby 2018 (1.kolo)
U kandidátů na prezidenta v prvním kole byly některé větší skupiny společných voličů:
- Drahoš: ODS, Piráti, STAN
- Fischer: KDU-ČSL
- Hilšer: Piráti, nevoliči v krajských volbách
- Horáček: Piráti, ODS, nevoliči
- Zeman: nevoliči, ANO, ČSSD

<table>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_pres2018-1_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_pres2018-1_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_pres2018-1_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_pres2018-1_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_pres2018-1_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_pres2018-1_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_pres2018-1_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_pres2018-1_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_pres2018-1_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_pres2018-1_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_pres2018-1_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_pres2018-1_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_pres2018-1_500x500.png"/></td>
    </tr>
</table>

### Parlamentní volby 2017
Viz popis výše u [učasti voličů z parlamentních voleb 2017](#Voliči-z-parlamentních-voleb-2017)

<table>
<tr>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_psp2017_500x500.png"/></td>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_psp2017_500x500.png"/></td>
</tr>
<tr>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_psp2017_500x500.png"/></td>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_psp2017_500x500.png"/></td>
</tr>
<tr>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_psp2017_500x500.png"/></td>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_psp2017_500x500.png"/></td>
</tr>
<tr>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_psp2017_500x500.png"/></td>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_psp2017_500x500.png"/></td>
</tr>
<tr>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_psp2017_500x500.png"/></td>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_psp2017_500x500.png"/></td>
</tr>
<tr>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_psp2017_500x500.png"/></td>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_psp2017_500x500.png"/></td>
</tr>
<tr>
    <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_psp2017_500x500.png"/></td>
</tr>
</table>

### Krajské volby 2016
V tomto případě se některé přesuny dají zobecnit a některé jsou ryze krajské.

- Voliči ČSSD a KSČM z před 4 let často nyní (krom volby stejné strany jako minule): nešli volit nebo volili ANO. Někde se to projevilo více (např. Jihomoravský nebo Moravskoslezský kraj) a někde méně (např. Pardubický nebo Vysočina)
- v Libereckém kraji, který se asi nejvíce vymyká zbytku ČR, je nejvíce dominantní skupina, která zůstala věrná SLK.


<table>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_kraje2016_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_kraje2016_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_kraje2016_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_kraje2016_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_kraje2016_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_kraje2016_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_kraje2016_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_kraje2016_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_kraje2016_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_kraje2016_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_kraje2016_500x500.png"/></td>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_kraje2016_500x500.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_kraje2016_500x500.png"/></td>
    </tr>
</table>

## Grafy ke stažení

| Kraj | Interaktivní graf | SVG 1000 x 900 | PNG Velikost Facebook 1360 x 1360 | PNG Velikost Twitter 1014 x 570|
| ---- | ---- | ---- | ---- | ---- |
| Středočeský kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ020_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ020_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ020_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ020_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ020_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ020_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ020_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ020_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ020_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ020_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ020_kraje2020_euro2019_1014x570.png) |
| Jihočeský kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ031_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ031_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ031_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ031_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ031_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ031_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ031_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ031_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ031_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ031_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ031_kraje2020_euro2019_1014x570.png) |
| Plzeňský kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ032_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ032_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ032_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ032_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ032_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ032_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ032_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ032_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ032_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ032_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ032_kraje2020_euro2019_1014x570.png) |
| Ústecký kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ041_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ041_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ041_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ041_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ041_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ041_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ041_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ041_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ041_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ041_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ041_kraje2020_euro2019_1014x570.png) |
| Karlovarský kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ042_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ042_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ042_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ042_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ042_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ042_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ042_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ042_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ042_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ042_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ042_kraje2020_euro2019_1014x570.png) |
| Liberecký kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ051_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ051_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ051_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ051_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ051_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ051_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ051_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ051_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ051_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ051_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ051_kraje2020_euro2019_1014x570.png) |
| Královéhradecký kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ052_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ052_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ052_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ052_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ052_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ052_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ052_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ052_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ052_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ052_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ052_kraje2020_euro2019_1014x570.png) |
| Pardubický kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ053_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ053_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ053_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ053_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ053_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ053_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ053_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ053_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ053_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ053_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ053_kraje2020_euro2019_1014x570.png) |
| Kraj Vysočina | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ063_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ063_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ063_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ063_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ063_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ063_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ063_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ063_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ063_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ063_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ063_kraje2020_euro2019_1014x570.png) |
| Jihomoravský kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ064_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ064_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ064_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ064_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ064_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ064_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ064_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ064_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ064_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ064_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ064_kraje2020_euro2019_1014x570.png) |
| Olomoucký kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ071_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ071_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ071_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ071_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ071_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ071_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ071_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ071_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ071_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ071_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ071_kraje2020_euro2019_1014x570.png) |
| Zlínský kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ072_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ072_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ072_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ072_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ072_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ072_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ072_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ072_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ072_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ072_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ072_kraje2020_euro2019_1014x570.png) |
| Moravskoslezský kraj | [Kraje 2016](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ080_kraje2020_kraje2016_500.html), [Prezident 2018 / 1.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ080_kraje2020_pres2018-1_500.html), [Prezident 2018 / 2.kolo](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ080_kraje2020_pres2018-2_500.html), [Sněmovna 2017](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ080_kraje2020_psp2017_500.html), [Euro 2019](https://volebnikalkulacka.cz/docs/ei/volby-2020/CZ080_kraje2020_euro2019_500.html) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ080_kraje2020_kraje2016_1000_900.svg), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ080_kraje2020_pres2018-1_1000_900.svg), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ080_kraje2020_pres2018-2_1000_900.svg), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ080_kraje2020_psp2017_1000_900.svg), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/CZ080_kraje2020_euro2019_1000_900.svg) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_kraje2016_1360x1360.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_pres2018-1_1360x1360.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_pres2018-2_1360x1360.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_psp2017_1360x1360.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_euro2019_1360x1360.png) | [Kraje 2016](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_kraje2016_1014x570.png), [Prezident 2018 / 1.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_pres2018-1_1014x570.png), [Prezident 2018 / 2.kolo](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_pres2018-2_1014x570.png), [Sněmovna 2017](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_psp2017_1014x570.png), [Euro 2019](https://gitlab.com/michalskop/volby-2020/-/raw/master/ei/kraje/images/ei_CZ080_kraje2020_euro2019_1014x570.png) |

## Díky ♥
Díky za podporu, která umožnila vznik této analýzy:
- [Skvělý a jediný patron takovýchto analýz na Patreonu](https://www.patreon.com/michalskop)
- [Přispěvatelé na projekt Mandáty.cz](https://www.darujme.cz/projekt/1200738)
- [Přispěvatelé na projekt VolebníKalkulačka.cz](https://www.darujme.cz/projekt/1200653)


## Další projekty autora
### Mandáty cz
[Mandáty.cz](https://mandaty.cz/) - aktualizovaný model pro volby do Sněmovny. Přepočteno na mandáty a možné koalice - na základě průzkumů.)

![Mandaty.cz](https://mandaty.cz/image/20200925162035.png)

### Volební atlas
[VolebníAtlas.cz](https://volebniatlas.cz/) - Detailní výsledky voleb na interaktivních mapách po volebních obvodech.

![Volební atlas](https://volebniatlas.cz/illustration.png)

### Volební kalkulačka
[Volební kalkulačka](https://volebnikalkulacka.cz) - Nejužitečnějších 5 minut před volbami.

[CC-BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/) Michal Škop 2020